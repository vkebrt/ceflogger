package ceflogger

import (
	"time"
)

/*
 * CEF SPECIFICATION:
 * Jan 18 11:07:53 host CEF:Version|Device Vendor|Device Product|Device
 * Version|Device Event Class ID|Name|Severity|[Extension]
 * FROM: https://www.microfocus.com/documentation/arcsight/arcsight-smartconnectors/pdfdoc/common-event-format-v25/common-event-format-v25.pdf
 */

type CEF struct {
	Timestamp          time.Time
	TimeFormat         string
	Host               string
	CEFVersion         string
	DeviceVendor       string
	DeviceProduct      string
	DeviceVersion      string
	DeviceEventClassID string
	EventName          string
	Severity           string
	Extension          []byte
}

//func NewCefNow - generate cef with actual timestamp, without any another vars
func NewCEFNow() *CEF {
	//aTime = time.Now()
	return &CEF{
		Timestamp:  time.Now(),
		TimeFormat: "Jan 12 11:07:53",
		CEFVersion: "1.0",
	}
}

// NewCEF - setup new cef structure, with specified time
func NewCEF(timex time.Time) *CEF {
	return &CEF{
		Timestamp:  timex,
		TimeFormat: "Jan 12 11:07:53",
		CEFVersion: "1.0",
	}
}

// SetCefHost - setup cef HOST part of header
func (c *CEF) SetCEFHost(host string) {
	c.Host = host
}

// SetDeviceVendor - setup cef DeviceVendor part of CEF
func (c *CEF) SetDeviceVendor(vendor string) {
	c.DeviceVendor = vendor
}

// SetDeviceProduct - setup cef DeviceProduct part of CEF
func (c *CEF) SetDeviceProduct(product string) {
	c.DeviceProduct = product
}

// SetDeviceVersion - setup cef DeviceVersion part of CEF
func (c *CEF) SetDeviceVersion(ver string) {
	c.DeviceVersion = ver
}

// SetDeviceECID - setup cef Device Event Class ID part of CEF
func (c *CEF) SetDeviceECID(ecid string) {
	c.DeviceEventClassID = ecid
}

// SetEventName - setup name part of CEF
func (c *CEF) SetEventName(en string) {
	c.EventName = en
}

// SetSeverity - strup severity part of CEF
func (c *CEF) SetSeverity(sev string) {
	c.Severity = sev
}

// AppendExt - Append Extension as byte array(convert from string)
func (c *Cef) AppendExt(ext string) {
	c.Extension = []byte(ext)
}


